<?php

namespace App\Http\Controllers;

use App\Http\Requests\Task\TaskRequest;
use App\Http\Resources\TaskResource;
use App\Models\Task;

class TaskController extends Controller
{
    public function list()
    {
        $tasks = Task::query()->paginate();

        return TaskResource::collection($tasks);
    }

    public function create(TaskRequest $request)
    {
        $taskData = $request->only([
            'title',
            'description',
            'user_id',
        ]);

        $task = Task::create($taskData);

        return TaskResource::make($task);
    }

    public function read(Task $task)
    {
        return TaskResource::make($task);
    }

    public function update(Task $task, TaskRequest $request)
    {
        $taskData = $request->only([
            'title',
            'description',
            'user_id',
        ]);

        $task->update($taskData);

        return TaskResource::make($task);
    }

    public function delete(Task $task)
    {
        $task->delete();

        return $this->list();
    }
}
