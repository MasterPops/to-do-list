<?php

namespace App\Http\Resources;

use App\Models\Task;
use App\Models\User;
use App\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

class TaskResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'task';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'tasks';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof Task) {
            return [
                'id' => $this->resource->id,
                'title' => $this->resource->title,
                'description' => $this->resource->description,
                'user_id' => $this->resource->user_id,
                'user' => new UserResource($this->resource->user),
            ];
        }

        return parent::toArray($request);
    }
}
