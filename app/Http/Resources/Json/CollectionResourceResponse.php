<?php

namespace App\Http\Resources\Json;

use Illuminate\Http\Resources\Json\ResourceResponse;

class CollectionResourceResponse extends ResourceResponse
{

    /**
     * Get the default data wrapper for the resource.
     *
     * @return string
     */
    protected function wrapper()
    {
        if ($this->resource instanceof AnonymousResourceCollection) {
            return $this->resource->wrapper();
        }
        return parent::wrapper();
    }

}
