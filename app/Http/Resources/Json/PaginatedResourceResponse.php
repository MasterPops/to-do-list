<?php

namespace App\Http\Resources\Json;

use Illuminate\Http\Resources\Json\PaginatedResourceResponse as IlluminatePaginatedResourceResponse;

class PaginatedResourceResponse extends IlluminatePaginatedResourceResponse
{

    /**
     * Gather the meta data for the response.
     *
     * @param array $paginated
     * @return array
     */
    protected function meta($paginated)
    {
        return [
            'path' => $paginated['path'],
            'page' => [
                'number' => $paginated['current_page'],
                'size' => $paginated['per_page'],
                'entries' => $this->resource->count(),
                'total' => $paginated['last_page'],
            ],
            'entries' => [
                'from' => $paginated['from'],
                'to' => $paginated['to'],
                'total' => $paginated['total'],
            ],
        ];
    }

    /**
     * Get the default data wrapper for the resource.
     *
     * @return string
     */
    protected function wrapper()
    {
        if ($this->resource instanceof AnonymousResourceCollection) {
            return $this->resource->wrapper();
        }
        return parent::wrapper();
    }

}
