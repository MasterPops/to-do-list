<?php

namespace App\Http\Middleware\Plugins;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AppendExtraKey
{
    /**
     * Handle an incoming request.
     *
     * @param Closure(Request): (Response) $next
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $data = $response->getContent();
        $data = json_decode($data, true);
        $data['extra_key'] = 'exctra_key_value';
        $response->setData($data);

        return $response;
    }
}
