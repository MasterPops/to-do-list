<?php

namespace App\Http\Middleware\Plugins;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class LogRequests
{
    /**
     * Handle an incoming request.
     *
     * @param Closure(Request): (Response) $next
     */
    public function handle($request, Closure $next)
    {
        Log::info('Request: ' . $request->fullUrl());

        return $next($request);
    }
}
