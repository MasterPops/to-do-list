<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::group(['middleware' => 'guest'], function () {
        Route::post('register', [AuthController::class, 'register']);
        Route::post('login', [AuthController::class, 'login']);
    });
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::post('logout', [AuthController::class, 'logout']);
    });
});


Route::group([
    'prefix' => 'task',
    'middleware' => 'auth:sanctum'
], function () {
        Route::get('', [TaskController::class, 'list']);
        Route::post('', [TaskController::class, 'create']);
        Route::get('/{task_id}', [TaskController::class, 'read']);
        Route::patch('/{task_id}', [TaskController::class, 'update']);
        Route::delete('/{task_id}', [TaskController::class, 'delete']);
});
